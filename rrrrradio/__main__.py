import sys
import asyncio

from .player import Player
from .util import async_prompt, async_print

player = Player()

async def interact():
    while True:
        [cmd, *args] = (await async_prompt(":) ")).split(" ")
        if cmd == "/queue" or cmd == "/q":
            await player.queue(args[0])
        elif cmd == "/skip" or cmd == "/s":
            await player.skip()
        elif cmd == "/help":
            print("Commands: /queue /skip /help")


if sys.platform == "win32":
    loop = asyncio.ProactorEventLoop()
    asyncio.set_event_loop(loop)
else:
    loop = asyncio.get_event_loop()

loop.run_until_complete(asyncio.gather(
    player(),
    interact()
))
loop.close()
