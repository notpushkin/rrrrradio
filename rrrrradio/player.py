import asyncio
from functools import partial

from youtube_dl import YoutubeDL


class Player:
    def __init__(self):
        self.urls = asyncio.Queue()
        self.media_urls = asyncio.Queue()
        self.proc = None

    @staticmethod
    def _mkpipeline(url):
        return [
            'souphttpsrc',
                'user-agent=Mozilla/5.0 (X11; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0.2',
                f'location={url}', '!',
            'decodebin', '!',

            # Audio may come in different sample rates, but LiqSoap doesn't
            # play well with that, so let's resample to a constant rate:
            'audioresample', '!',
            'audio/x-raw, rate=48000', '!',

            'audioconvert', '!',
            'wavpackenc', '!',
            'udpsink',
                'host=127.0.0.1',
                'port=5004',
        ]

    async def _control_gstreamer_process(self):
        while True:
            url = await self.media_urls.get()
            self.proc = await asyncio.create_subprocess_exec(
                "gst-launch-1.0",
                *self._mkpipeline(url),
                # stdout=asyncio.subprocess.DEVNULL,
                # stderr=asyncio.subprocess.DEVNULL
            )
            await self.proc.wait()
            self.proc = None

    async def _process_urls(self):
        loop = asyncio.get_event_loop()
        while True:
            url = await self.urls.get()
            with YoutubeDL({ "quiet": True }) as ydl:
                f = partial(ydl.extract_info, url, download=False)
                info_dict = await loop.run_in_executor(None, f)

                if info_dict.get("requested_formats") is not None:
                    f = info_dict["requested_formats"][-1]
                    media_url = f["url"] + f.get("play_path", "")
                else:
                    # For RTMP URLs, also include the playpath
                    media_url = info_dict["url"] + info_dict.get("play_path", "")

                await self.media_urls.put(media_url)

    def __call__(self):
        return asyncio.gather(
            self._control_gstreamer_process(),
            self._process_urls(),
        )

    async def queue(self, url):
        await self.urls.put(url)

    async def skip(self):
        if self.proc is not None:
            self.proc.terminate()
            self.proc = None
