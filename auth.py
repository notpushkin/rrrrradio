#!/usr/bin/python3
import sys


def auth(password):
    return password


if __name__ == '__main__':
    print(auth(sys.argv[1]) or "__unauthorized__")
