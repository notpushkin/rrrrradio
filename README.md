# rrrrradio

Requirements:

* Liquidsoap (1.1.1 recommended) with some plugins (?)
* Python 3.6+

```bash
apt-get install liquidsoap liquidsoap-plugin-all python3
pip3 install youtube-dl
```

## Usage

```bash
liquidsoap app.liq
# in separate window:
python3 -m rrrrradio
```

**rrrrr** reads some env variables. You can do:

```bash
export PORT=8841
export ICECAST_HOST=example.net
export ICECAST_PORT=8080
export ICECAST_PASSWORD='something-str0ng3r_th@N-"hackme"'
export RADIO_URL="https://goo.gl/loz2gq"
export RADIO_DESCRIPTION="my cool streabm !!!11"
liquidsoap app.liq
```

## Live streams

You need an Icecast-compatible client to stream to **rrrrr**.

* **Username:** *any* \*
* **Password:** *desired username*
* **Mount:** `/live`

\* — some Icecast clients have hardcoded username `source`, so we just ignore
this field altogether.

You can modify the authorization scheme by changing the [auth.py](auth.py) file.


## Prior Art

The Liquidsoap part is mostly a mixture of various developers' code:

- [densetos' Provodach](https://dev.asterleen.com/cat/radio/) (in Russian) — actually the project that made me want to study Internet radios :3
- [djazz's Paraspite Radio](https://github.com/daniel-j/parasprite-radio/)
- [C. Schirnen's mopidy-stream](https://github.com/schinken/docker-container/blob/master/mopidy-stream/)

GStreamer part takes some ideas from [Mopidy](https://www.mopidy.com/) (and can
be replaced with it, if needed – just use `wavpackenc ! udpsink host=127.0.0.1
port=5004` as a sink.)
